#Autor:Reyes Almanza Jesus Abraham
#!/bin/bash

n=4
echo "*********MOSTRANDO TAMAÑO DE LA MATRIZ Y TIEMPO EN SEGUNDOS.***********"
echo -e "|Secuencial\t|TBB\t\t|Cilk\t\t|Pthreads\t|Procesos\t|"
echo "-----------------------------------------------------------------------------------"
for (( i=4; i<=500; i++ ))
do
	if !(($i%50))
	then
			echo -e "|$(./mergeSortNormal $i)\t|$(./mergeSortTBB $n $i)\t|$(./mergeSortCilk $n $i)\t|$(./mergeSortPthreads $n $i)\t|$(./mergeSort2Processes $i)\t|"
	fi
done
echo "Generado datos y gráfica..."
touch TBB.dat
touch Cilk.dat
touch procesos.dat
touch pthreads.dat
touch normal.dat
n=4 
for (( i=4; i<=500; i+=2 ))
do
	if !(($i%2))
	then	
		./mergeSortNormal $i >>normal.dat
		./mergeSortTBB $n $i >>TBB.dat
		./mergeSortCilk $n $i >>Cilk.dat
		./mergeSortPthreads $n $i>>pthreads.dat
		./mergeSort2Processes $i >>procesos.dat
	fi
done
gnuplot -e "set term pdf; set out 'plot.pdf'; set xlabel 'Matrix Size'; set ylabel 'Time(s)'; plot 'Cilk.dat' with lines title 'Cilk', 'TBB.dat' with lines title 'TBB', 'procesos.dat' with lines title 'Procesos', 'pthreads.dat' with lines title 'Pthreads', 'normal.dat' with lines title 'Secuencial'"
rm mergeSortNormal
rm mergeSortTBB
rm mergeSortCilk
rm mergeSortPthreads
rm mergeSort2Processes
#rm *.dat
