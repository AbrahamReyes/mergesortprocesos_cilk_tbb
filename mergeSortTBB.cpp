/*Compilacion g++ mergeSortTBB.cpp -o mergeSortTBB -ltbb
 *Autor:Jesús Abraham Reyes Almanza
 * */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <chrono>
#include <string.h>
#include "tbb/task_scheduler_init.h"
#include "tbb/blocked_range.h"
#include "tbb/parallel_for.h"
#include "sortLib.h"


using namespace std;
using namespace tbb;

std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;	
struct partial_merge{
	int *matrix;
	int *partition;
	void operator()(const blocked_range<int>& range )const{
		for(int i = range.begin();i!=range.end();++i)
			merge_sort(matrix,(*partition)*i, (*partition)*i+(*partition)-1);
	} 
};
void partial_merge_parallel(int *matrix,int *partition, size_t nthread){
	partial_merge matrixMerge;
	matrixMerge.matrix = matrix;
	matrixMerge.partition = partition;
	parallel_for(blocked_range<int>(0,nthread),matrixMerge);
}
/*void partial_merge(int *matrix,int partition,int nthread){

	for(int idthread=0;idthread<nthread;idthread++){
		merge_sort(matrix,partition*idthread, partition*idthread+partition-1);
	}
}
*/
int main(int argc, char *argv[]){
	if (argc!=3){
		cout<<"<Nombre del programa> <#threads> <Tamaño de la matriz>"<<endl;
		exit(1);
	}
	int nthread = atoi(argv[1]);
	int N = atoi(argv[2]);	
	//variables para medir el tiempo
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;	

	int *matrix;
	int n = N/nthread;
	int partition =(N*N)/nthread;
	matrix = (int*)malloc(N*N*sizeof(int));
	memset(matrix,0,N*N*sizeof(int));

	/****Llenamos la Matriz con números aleatorios****/
	srand(time(NULL));
	int i,j;
	for(i = 0;i<N;i++){
		for(j = 0; j<N;j++){
			matrix[i*N+j]=rand()%10;
		}
	}
	
	task_scheduler_init init(task_scheduler_init::deferred);
	if( nthread>=1 ) 
		init.initialize(nthread);
	
	start_clock = std::chrono::system_clock::now();		
	partial_merge_parallel(matrix,&partition,(size_t)nthread);
	lineal_mergeSort(matrix,0,N*N-1,nthread,N);
	end_clock = std::chrono::system_clock::now();

	std::chrono::duration<double> elapsed_seconds = end_clock-start_clock;


	/*for(int i=0;i<N;i++){
		for(int j=0;j<N;j++){
			cout<<matrix[i*N+j]<<" ";
		}
		cout<<endl;
	}
	cout<<"Tiempo total: "<<elapsed_seconds.count()<<endl;*/
	cout<<N<<" "<<elapsed_seconds.count()<<endl;
	if( nthread<=1 )
 		init.terminate();

	return 0;

}


