/*Compilacion: g++ mergeSortPthreads.cpp -pthread -o mergeSortPthreads
 *Autor: Reyes Almanza Jesus Abraham
 * */
#include <stdio.h>
#include <iostream>
#include <time.h>
#include <chrono>
#include "sortLib.h"

using namespace std;
pthread_mutex_t mutex;
int step_i = 0; 


struct thread_data{
	int *Array;
	int p;
	int r;
};

void *sort(void *input){
	struct thread_data *mydata;

	mydata = (struct thread_data *) input;

	int p = mydata->p;
	int r = mydata->r;
	int *A = mydata->Array;
	pthread_mutex_lock(&mutex);
	merge_sort(A,p,r);
	pthread_mutex_unlock(&mutex);

 
	pthread_exit(NULL);
}

int main(int argc, char **argv){
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;	
	if(argc!=3){
		cout << "Uso: ./<Nombre del ejecutable> <# de Threads> <Tamaño de la matriz>"<<endl;
		exit(1);	
	}
	int rc;
	int MAX = atoi(argv[2]);
	int nthreads = atoi(argv[1]);


		
	pthread_mutex_init(&mutex, NULL);
	int matA[MAX][MAX]; 

	srand(time(NULL));
	for (int i = 0; i < MAX; i++) { 
    	    for (int j = 0; j < MAX; j++) { 
        	    matA[i][j] = rand() % 10; 
        	}	 
    }

	   // Displaying matA 
    /*cout << endl 
         << "Matrix A" << endl; 
    for (int i = 0; i < MAX; i++) { 
        for (int j = 0; j < MAX; j++)  
            cout << matA[i][j] << " "; 
        cout << endl; 
    } */
  
	pthread_t threads[nthreads];
	pthread_attr_t attr;
	void *status;
	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	
	struct thread_data td[nthreads];

	int submax = (MAX*MAX)/nthreads;//tamaño del subarreglo a ordenar es igual al total de entradas de la matriz entre el numero de hilos

	
	pthread_mutex_init(&mutex, NULL);

	start_clock = std::chrono::system_clock::now();		
	for (int i=0; i<nthreads;i++){
		
		td[i].Array = (int*) matA;
		td[i].p = i*submax;
		td[i].r = td[i].p + (submax-1);
		pthread_create(&threads[i], NULL, sort, (void*)&td[i]);
	}

	/*cout << endl 
         << "Matrix A" << endl; 
    for (int i = 0; i < MAX; i++) { 
        for (int j = 0; j < MAX; j++)  
            cout << matA[i][j] << " "; 
        cout << endl; 
    }*/
    // joining and waiting for all threads to complete 
    for (int i = 0; i < nthreads; i++)  
        pthread_join(threads[i], NULL);
    
	int r=0,p=0,q=0;
	int subMAX = submax*2; //tamaño del arreglo restante al que se le aplica merge
	int tex_m=nthreads/2;//numero de veces restantes de ejecutar merge;

	/*for(subMAX;subMAX<=MAX*MAX;subMAX*=2, tex_m/=2){
			for(int i=0;i<tex_m;i++){
				p = i*subMAX;
				r= p+subMAX-1;//
				q = (p+r)/2;
				merge((int *)matA,p,q,r);
			}
	}*/
	lineal_mergeSort((int *)matA, 0, MAX*MAX-1,nthreads,MAX);
	
	end_clock = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_clock-start_clock;

	/*
	cout << endl 
         << "Matrix A" << endl; 
    for (int i = 0; i < MAX; i++) { 
        for (int j = 0; j < MAX; j++)  
            cout << matA[i][j] << " "; 
        cout << endl; 
    }
	*/
    cout <<MAX<<" "<<elapsed_seconds.count()<<endl;
	pthread_exit(NULL);

}


