/*Compilacion g++ mergeSortCilk.cpp -fcilkplus -o mergeSortCilk*/
/*Autor: Reyes Almanza Jesús Abraham*/
/*Este programa realiza el ordenamiento de una matriz usando merge sort en paralelo distribuyendo el trabajo de ordenamiento en los núcleos.*//*Divide la matriz en partes y las ordena en paralelo despues se ordenan las partes ordenadas de manera secuencial.*/
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <chrono>
#include <string.h>
#include <cilk/cilk.h>
#include "sortLib.h"

void partial_merge_parallel(int *matrix,int partition,int chunks){

	cilk_for (int idchunk=0;idchunk<chunks;idchunk++){
		merge_sort(matrix,partition*idchunk, partition*idchunk+partition-1);
	}
}
using namespace std;
int main(int argc, char *argv[]){
	if (argc!=3){
		cout<<"Este programa realiza el ordenamiento de una matriz usando merge sort en paralelo distribuyendo el trabajo de ordenamiento en los núcleos.*//*Divide la matriz en partes y las ordena en paralelo despues se ordenan las partes ordenadas de manera secuencial."<<endl;
		cout<<endl;
		cout<<"<Nombre del programa> <#partes> <Tamaño de la matriz>"<<endl;
		exit(1);
	}
	int parts = atoi(argv[1]);
	int N = atoi(argv[2]);	
	//variables para medir el tiempo
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;	

	int *matrix;
	int n = N/parts;
	int partition =(N*N)/parts;
	matrix = (int*)malloc(N*N*sizeof(int));
	memset(matrix,0,N*N*sizeof(int));

	/****Llenamos la Matriz con números aleatorios****/
	//srand(time(NULL));
	srand(1000);
	int i,j;
	for(i = 0;i<N;i++){
		for(j = 0; j<N;j++){
			matrix[i*N+j]=rand()%10;
		}
	}
	
	
	start_clock = std::chrono::system_clock::now();		
	partial_merge_parallel(matrix,partition,parts);
	lineal_mergeSort(matrix,0,N*N-1,parts,N);
	end_clock = std::chrono::system_clock::now();

	std::chrono::duration<double> elapsed_seconds = end_clock-start_clock;


	/*for(int i=0;i<N;i++){
		for(int j=0;j<N;j++){
			cout<<matrix[i*N+j]<<" ";
		}
		cout<<endl;
	}*
	cout<<"Tiempo total: "<<elapsed_seconds.count()<<endl;*/
	cout<<N<<" "<<elapsed_seconds.count()<<endl;

	return 0;

}
