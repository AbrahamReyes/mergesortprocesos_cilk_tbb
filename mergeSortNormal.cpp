/*Compilacion g++ mergesortNormal.cpp -o mergeSortNormal
 *Autor: Reyes Almanza Jesus Abraham
 * */
#include <stdio.h>
#include <chrono>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include "sortLib.h"

// maximum size of matrix  
  
// maximum number of threads 
  
int step_i = 0; 
pthread_mutex_t mutex;

using namespace std;

int main(int argc, char **argv){
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;	
	if(argc!=2){
		cout << "Uso: ./<Nombre del ejecutable> <Tamaño de la matriz>"<<endl;
		exit(1);	
	}
	int MAX = atoi(argv[1]);
	
	int matA[MAX][MAX]; 



	srand(time(NULL));
	for (int i = 0; i < MAX; i++) { 
    	    for (int j = 0; j < MAX; j++) { 
        	    matA[i][j] = rand() % 10; 
        	}	 
    }





/*	cout << endl << "Matrix Bfore Merge" << endl;
	// Displaying matA 
    for (int i = 0; i < MAX; i++) { 
        for (int j = 0; j < MAX; j++)  
            cout << matA[i][j] << " "; 
        cout << endl; 
    }*/ 
	start_clock = std::chrono::system_clock::now();		
	merge_sort((int *)matA,0,MAX*MAX-1); 
	end_clock = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_clock-start_clock;


	  //Display A after Merge  
	 
/*	cout << endl << "Matrix After Merge" << endl;
	// Displaying matA 
   	for (int i = 0; i < MAX; i++) { 
        for (int j = 0; j < MAX; j++)  
            cout << matA[i][j] << " "; 
        cout << endl; 
    } */
    cout <<MAX<<" "<<elapsed_seconds.count()<<endl;

return 0;
}
