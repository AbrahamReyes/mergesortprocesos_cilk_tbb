# MergeSortProcesos_Cilk_TBB

Part 1 of the course in High Perfomance Computing in Cinvestav Zacatenco México. En este proyecto se realiza el ordenamiento en paralelo de una matriz con numeros enteros. El trabajo en los programas se realiza usando el algoritmo _merge sort_. Se divide la matriz en pedazos que se ordenan en paralelo y cuando están ordenados se ordenan de manera secuncial los restantes pedazos.
### Requisitos
CilkPlus
TBB
gcc
gnuplot
## Compilacion
```
make
```
## Ejecución
```
bash ./ejecutarPrograma 
```
## Autor

_Reyes Almanza Jesús Abraham_
jreyes@computacion.cs.cinvestav.mx
