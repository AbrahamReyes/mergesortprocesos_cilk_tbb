all: mergeSortTBB mergeSortCilk mergeSort2Processes mergeSortPthreads mergeSortNormal clean

mergeSortTBB.o: mergeSortTBB.cpp sortLib.h
	g++ -c -g -ltbb mergeSortTBB.cpp 
mergeSortCilk.o: mergeSortCilk.cpp sortLib.h
	g++ -c -g mergeSortCilk.cpp -fcilkplus
mergeSortTBB.o: mergeSort2Processes.cpp sortLib.h
	g++ -c -g mergeSortTBB.cpp 
mergeSortPthreads.o: mergeSortPthreads.cpp sortLib.h
	g++ -c -g mergeSortPthreads.cpp -pthread
mergeSortNormal.o: mergeSortNormal.cpp sortLib.h
	g++ -c -g mergeSortNormal.cpp	

mergeSortTBB: mergeSortTBB.o
	g++ -g -o mergeSortTBB mergeSortTBB.o -ltbb
mergeSortCilk: mergeSortCilk.o
	g++ -g -o mergeSortCilk mergeSortCilk.o -fcilkplus
mergeSort2Processes: mergeSort2Processes.o
	g++ -g -o mergeSort2Processes mergeSort2Processes.o
mergeSortPthreads: mergeSortPthreads.o
	g++ -g -o mergeSortPthreads mergeSortPthreads.o -pthread
mergeSortNormal: mergeSortNormal.o
	g++ -g -o mergeSortNormal mergeSortNormal.o	

clean :
	rm mergeSortTBB.o mergeSortCilk.o mergeSort2Processes.o mergeSortPthreads.o mergeSortNormal.o 
