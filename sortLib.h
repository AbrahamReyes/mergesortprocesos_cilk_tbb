void merge(int *A, int p, int q, int r){
//Supongase un arreglo |x_0|x_1|...|x_q|...|x_r|	
		
	int n1 = q-p+1, n2 = r-q;
	int L[n1+1], R[n2+1];

	int i=0, j=0;
	
	for(int k = 0; k<n1; k++){
		L[k] = A[p+k];
	}
	for(int k = 0; k< n2; k++){
		R[k] = A[q+k+1];
	}
	L[n1]=1155522;
	R[n2]=1155522;
	for(int k=p; k <=r;k++){
		if ( L[i] <= R[j]){
			A[k]=L[i];
			i = i+1;
		}
		else{
			A[k]=R[j];
			j = j + 1;
		} 	
	}
}
void merge_sort(int *A, int p, int r){
		if (p<r){
			int q=(p+r)/2;
			merge_sort(A,p,q);
			merge_sort(A,q+1,r);
			merge(A,p,q,r);
		}
}
void lineal_mergeSort(int *A, int p, int r, int stop, int N){
	if ((r-p+1)!=(N*N)/stop){
		int q = (p+r)/2;
		lineal_mergeSort(A,p,q,stop,N);
		lineal_mergeSort(A,q+1,r,stop,N);
		merge(A,p,q,r);
	}
}


