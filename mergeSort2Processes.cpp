/*compilacion: g++ mergesort2Processes.cpp -o mergesort2Processes
 *Autor:Reyes almanza Jesus Abraham
 * */
#define procesos 2

#include <stdio.h>
#include <stdlib.h>
#include <chrono>
#include <sys/wait.h>
#include <unistd.h>
#include <iostream>
#include <string.h>
#include "sortLib.h"
using namespace std;


int main(int argc, char **argv){
	if(argc!=2){
	cout<<"<Nombre del programa> <Tamaño de la matriz>"<<endl;
	exit(-1);	
	}
	int N = atoi(argv[1]);	
	//variables para medir el tiempo
	std::chrono::time_point<std::chrono::system_clock> start_clock, end_clock;	

	int *matrix;
	int fd1[2];
	int fd2[2];
	int n = N/2;
	int part = (N*N);
 	pid_t pid1;
	
	matrix = (int*)malloc(N*N*sizeof(int));
	memset(matrix,0,N*N*sizeof(int));

	/****Llenamos la Matriz con números aleatorios****/
	srand(time(NULL));
	int i,j;
	for(i = 0;i<N;i++){
		for(j = 0; j<N;j++){
			matrix[i*N+j]=rand()%10;
		}
	}
	if (pipe(fd1)==-1) 
    { 
        fprintf(stderr, "Pipe Failed" ); 
        return 1; 
    } 
    if (pipe(fd2)==-1) 
    { 
        fprintf(stderr, "Pipe Failed" ); 
        return 1; 
    } 
	//Creamos un proceso hijo
	
	start_clock = std::chrono::system_clock::now();		
	pid1=fork();

	if(pid1<0){
		cout<<"Fork failed"<<endl;
		return 1;
	}
	//Se realiza merge sort en diviendo la matriz en dos partes ordenando una parte en el procesos padre y la otra en el proceso hijo
	else if(pid1>0){

			merge_sort(matrix,0,n*N-1);
			close(fd1[0]);
			//Pasamos la matriz al proceso hijo
			write(fd1[1],matrix,sizeof(int)*N*N);
			close(fd1[1]);
			//wait(NULL);

			close(fd2[1]);
			//se recibe la matriz del proceso hijo
			read(fd2[0],matrix,N*N*sizeof(int));
			close(fd2[0]);
			//y se ordenan las dos partes
			lineal_mergeSort(matrix,0,N*N-1,procesos,N);
		/*for(i = 0;i<N;i++){
			for(j = 0; j<N;j++){
			cout<<matrix[i*N+j]<<" ";
			}
			cout<<endl;
		}*/			

	}
	else{
		close(fd1[1]);
		int *mmatrix;
		mmatrix = (int *)malloc(N*N*sizeof(int));
		//recibimos la matriz del proceso padre
		read(fd1[0],mmatrix,sizeof(int)*N*N);
		//ordenamos
		merge_sort(mmatrix,n*N,N*N);
		close(fd1[0]);
		close(fd2[0]);
		//la regresamos al proceso padre
		write(fd2[1],mmatrix,N*N*sizeof(int));
		close(fd2[1]);
		exit(0);
	
	}
	end_clock = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end_clock-start_clock;
	//cout<<"Tiempo total: "<<elapsed_seconds.count()<<endl;
	cout<<N<<" "<<elapsed_seconds.count()<<endl;
//cout<<"Fin del programa."<<endl;
return 0;
}
